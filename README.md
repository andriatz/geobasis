# geobasis

An humble geodata archive

* **Comuni** year 2020 -> https://gitlab.com/andriatz/geobasis/-/raw/master/italy/comuni_2020.geojson
* **Province** year 2020 *(generated from Comuni)* <- https://gitlab.com/andriatz/geobasis/-/raw/master/italy/province_2020(s).geojson
* **Regioni** year 2020 *(generated from Comuni)* <- https://gitlab.com/andriatz/geobasis/-/raw/master/italy/regioni_2020(s).geojson 
